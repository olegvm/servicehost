﻿using System;

namespace Oxp.ServiceModel.Events
{
    public interface ISubscriber
    {
        ISubscriber Subscribe<T>(Action<T> onNext) where T : IEvent;
        ISubscriber Subscribe<T>(Action<T> onNext, Action<Exception> onError) where T : IEvent;
    }
}
