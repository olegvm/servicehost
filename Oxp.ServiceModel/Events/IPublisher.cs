﻿using System;

namespace Oxp.ServiceModel.Events
{
    public interface IPublisher
    {
        IPublisher Publish<T>(T @event) where T : IEvent;
    }
}
