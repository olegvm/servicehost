﻿using System;

namespace Oxp.ServiceModel.Events
{
    public class ThreadEvents
    {
        public class ThreadStart : EventBase<ThreadStart>, IThreadEvent, IEvent { }
        public class ThreadComplete : EventBase<ThreadComplete>, IThreadEvent, IEvent { }
        public class Abort : EventBase<Abort>, IThreadEvent, IEvent { }
        public class Cancel : EventBase<Cancel>, IThreadEvent, IEvent { }
        public class ExceptionHandled : ExceptionEvent<ExceptionHandled>, IExceptionEvent, IEvent { }

        public static IEvent OnThreadStart() { return ThreadStart.New.WithPrefix("\tThreadEvents."); }
        public static IEvent OnThreadComplete() { return ThreadComplete.New.WithPrefix("\tThreadEvents."); }
        public static IEvent OnAbort() { return Abort.New.WithPrefix("\tThreadEvents."); }
        public static IEvent OnCancel() { return Cancel.New.WithPrefix("\tThreadEvents."); }
        public static IEvent OnExceptionHandled(Exception ex) { return ExceptionHandled.New(ex).WithPrefix("\tThreadEvents."); }
    }
}
