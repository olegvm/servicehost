﻿using System;

namespace Oxp.ServiceModel.Events
{
    public class ExtensionEvents
    {
        public class ExceptionHandled : ExceptionEvent<ExceptionHandled>, IExceptionEvent, IEvent { }

        public static IEvent OnExceptionHandled(Exception ex) { return ExceptionHandled.New(ex).WithPrefix("ExtensionEvents."); }
    }
}
