﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Events
{
    public class ExceptionEvent<T> : EventBase<T>, IExceptionEvent, IEvent 
        where T : ExceptionEvent<T>, IExceptionEvent, IEvent, new()
    {
        #region EventBase<ExceptionEvent> members

        public T With(Exception ex = null, string name = null, DateTime dateTime = default(DateTime), Thread thread = null)
        {
            this.Exception = ex;

            return (T)base.With(name, dateTime, thread);
        }

        #endregion

        #region IExceptionEvent members

        public Exception Exception { get; private set; }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}{1}{2}", base.ToString(), Environment.NewLine, Exception);
        }

        public static new T New(Exception ex)
        {
            return new T().With(ex);
        }
    }
}
