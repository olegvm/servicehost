﻿using System;

namespace Oxp.ServiceModel.Events
{
    public interface IEventBus : IPublisher, ISubscriber, IDisposable
    {
    }
}
