﻿using System;

namespace Oxp.ServiceModel.Events
{
    public interface IExceptionEvent : IEvent
    {
        Exception Exception { get; }
    }
}
