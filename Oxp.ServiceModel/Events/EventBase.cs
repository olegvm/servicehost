﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Events
{
    public class EventBase<T> : IEvent where T : EventBase<T>, IEvent, new()
    {
        #region IEvent members

        public object Data { get; private set; }
        public string Name { get; private set; }
        public DateTime DateTime { get; private set; }
        public Thread Thread { get; private set; }

        #endregion

        protected virtual string BuildName()
        {
            return this.GetType().Name;
        }

        public virtual T WithPrefix(string prefix)
        {
            this.Name = string.Concat(prefix, BuildName());

            return (T)this;
        }

        public virtual T WithData(object data)
        {
            this.Data = data;

            return (T)this;
        }

        public virtual T With(string name = null, DateTime dateTime = default(DateTime), Thread thread = null)
        {
            this.Name = name ?? BuildName();
            this.DateTime = (dateTime == default(DateTime)) ? DateTime.UtcNow : dateTime;
            this.Thread = thread ?? Thread.CurrentThread;

            return (T)this;
        }

        public override string ToString()
        {
            return string.Format("[T:{0:00}][{2:yyyy-MM-dd HH:mm:ss.fff}] {1} {3}", Thread.ManagedThreadId, Name, DateTime, Data);
        }

        public static T New
        {
            get { return new T().With(); }
        }
    }
}
