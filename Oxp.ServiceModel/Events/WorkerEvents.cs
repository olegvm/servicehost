﻿using System;

namespace Oxp.ServiceModel.Events
{
    public class WorkerEvents
    {
        public class Creating         : EventBase<Creating>, IWorkerEvent, IEvent { }
        public class Created          : EventBase<Created>, IWorkerEvent, IEvent { }
        public class Initializing     : EventBase<Initializing>, IWorkerEvent, IEvent { }
        public class Initialized      : EventBase<Initialized>, IWorkerEvent, IEvent { }
        public class Waiting          : EventBase<Waiting>, IWorkerEvent, IEvent { }
        public class WaitComplete     : EventBase<WaitComplete>, IWorkerEvent, IEvent { }
        public class WorkReceiving    : EventBase<WorkReceiving>, IWorkerEvent, IEvent { }
        public class WorkReceived     : EventBase<WorkReceived>, IWorkerEvent, IEvent { }
        public class WorkPerforming   : EventBase<WorkPerforming>, IWorkerEvent, IEvent { }
        public class WorkPerformed    : EventBase<WorkPerformed>, IWorkerEvent, IEvent { }
        public class Disposing        : EventBase<Disposing>, IWorkerEvent, IEvent { }
        public class Disposed         : EventBase<Disposed>, IWorkerEvent, IEvent { }
        public class ExceptionHandled : ExceptionEvent<ExceptionHandled>, IExceptionEvent, IEvent { }

        public static IEvent OnCreating() { return Creating.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnCreated() { return Created.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnInitializing() { return Initializing.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnInitialized() { return Initialized.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnWaiting() { return Waiting.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnWaitComplete() { return WaitComplete.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnWorkReceiving() { return WorkReceiving.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnWorkReceived() { return WorkReceived.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnWorkPerforming() { return WorkPerforming.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnWorkPerformed() { return WorkPerformed.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnDisposing() { return Disposing.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnDisposed() { return Disposed.New.WithPrefix("\tWorkerEvents."); }
        public static IEvent OnExceptionHandled(Exception ex) { return ExceptionHandled.New(ex).WithPrefix("\tWorkerEvents."); }
    }
}
