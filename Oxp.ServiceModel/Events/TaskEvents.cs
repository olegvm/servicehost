﻿using System;

namespace Oxp.ServiceModel.Events
{
    public class TaskEvents
    {
        public class Trace : EventBase<Trace>, ITaskEvent, IEvent { }
        public class TraceVerbose : EventBase<TraceVerbose>, ITaskEvent, IEvent { }
        public class TraceInformation : EventBase<TraceInformation>, ITaskEvent, IEvent { }
        public class TraceWarning : EventBase<TraceWarning>, ITaskEvent, IEvent { }
        public class TraceError : EventBase<TraceError>, ITaskEvent, IEvent { }
        public class ExceptionHandled : ExceptionEvent<ExceptionHandled>, IExceptionEvent, IEvent { }

        public static IEvent OnTrace(object data) { return Trace.New.WithPrefix("\t\t\tTaskEvents.").WithData(data); }
        public static IEvent OnTrace(string format, object data) { return Trace.New.WithPrefix("\t\t\tTaskEvents.").WithData(string.Format(format, data)); }
        public static IEvent OnTraceVerbose(object data) { return TraceVerbose.New.WithPrefix("\t\t\tTaskEvents.").WithData(data); }
        public static IEvent OnTraceVerbose(string format, object data) { return TraceVerbose.New.WithPrefix("\t\t\tTaskEvents.").WithData(string.Format(format, data)); }
        public static IEvent OnTraceInformation(object data) { return TraceInformation.New.WithPrefix("\t\t\tTaskEvents.").WithData(data); }
        public static IEvent OnTraceInformation(string format, object data) { return TraceInformation.New.WithPrefix("\t\t\tTaskEvents.").WithData(string.Format(format, data)); }
        public static IEvent OnTraceWarning(object data) { return TraceWarning.New.WithPrefix("\t\t\tTaskEvents.").WithData(data); }
        public static IEvent OnTraceWarning(string format, object data) { return TraceWarning.New.WithPrefix("\t\t\tTaskEvents.").WithData(string.Format(format, data)); }
        public static IEvent OnTraceError(object data) { return TraceError.New.WithPrefix("\t\t\tTaskEvents.").WithData(data); }
        public static IEvent OnTraceError(string format, object data) { return TraceError.New.WithPrefix("\t\t\tTaskEvents.").WithData(string.Format(format, data)); }
        public static IEvent OnExceptionHandled(Exception ex) { return ExceptionHandled.New(ex).WithPrefix("\t\t\tTaskEvents."); }
    }
}
