﻿using System;

namespace Oxp.ServiceModel.Events
{
    public interface IHandler<in T> where T : IEvent
    {
        void Handle(T @event);
    }
}
