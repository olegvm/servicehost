﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Events
{
    public interface IEvent 
    {
        object Data { get; }
        string Name { get; }
        DateTime DateTime { get; }
        Thread Thread { get; }
    }
}
