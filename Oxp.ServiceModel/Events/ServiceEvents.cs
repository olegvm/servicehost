﻿using System;

namespace Oxp.ServiceModel.Events
{
    public class ServiceEvents
    {
        public class Starting         : EventBase<Starting>, IServiceEvent, IEvent { }
        public class Started          : EventBase<Starting>, IServiceEvent, IEvent { }
        public class Pausing          : EventBase<Pausing>, IServiceEvent, IEvent { }
        public class Paused           : EventBase<Paused>, IServiceEvent, IEvent { }
        public class Continuing       : EventBase<Continuing>, IServiceEvent, IEvent { }
        public class Continued        : EventBase<Continued>, IServiceEvent, IEvent { }
        public class Stopping         : EventBase<Stopping>, IServiceEvent, IEvent { }
        public class Stopped          : EventBase<Stopped>, IServiceEvent, IEvent { }
        public class Disposing        : EventBase<Disposing>, IServiceEvent, IEvent { }
        public class Disposed         : EventBase<Disposed>, IServiceEvent, IEvent { }
        public class ExceptionHandled : ExceptionEvent<ExceptionHandled>, IExceptionEvent, IEvent { }

        public static IEvent OnStarting() { return Starting.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnStarted() { return Started.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnPausing() { return Pausing.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnPaused() { return Paused.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnContinuing() { return Continuing.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnContinued() { return Continued.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnStopping() { return Stopping.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnStopped() { return Stopped.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnDisposing() { return Disposing.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnDisposed() { return Disposed.New.WithPrefix("ServiceEvents."); }
        public static IEvent OnExceptionHandled(Exception ex) { return ExceptionHandled.New(ex).WithPrefix("ServiceEvents."); }
    }
}