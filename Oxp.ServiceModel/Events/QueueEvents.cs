﻿using System;

namespace Oxp.ServiceModel.Events
{
    public class QueueEvents
    {
        public class Creating : EventBase<Creating>, IQueueEvent, IEvent { }
        public class Created : EventBase<Created>, IQueueEvent, IEvent { }
        public class TaskProducing : EventBase<TaskProducing>, IQueueEvent, IEvent { }
        public class TaskProduced : EventBase<TaskProduced>, IQueueEvent, IEvent { }
        public class NewTaskCreated : EventBase<NewTaskCreated>, IQueueEvent, IEvent { }
        public class CompleteTaskRemoved : EventBase<CompleteTaskRemoved>, IQueueEvent, IEvent { }
        public class CapacityExceeded : EventBase<CapacityExceeded>, IQueueEvent, IEvent { }
        public class QueueTryAddError : EventBase<QueueTryAddError>, IQueueEvent, IEvent { }
        public class TaskConsuming : EventBase<TaskConsuming>, IQueueEvent, IEvent { }
        public class TaskConsumed : EventBase<TaskConsumed>, IQueueEvent, IEvent { }
        public class Disposing : EventBase<Disposing>, IQueueEvent, IEvent { }
        public class Disposed : EventBase<Disposed>, IQueueEvent, IEvent { }
        public class ExceptionHandled : ExceptionEvent<ExceptionHandled>, IExceptionEvent, IEvent { }

        public static IEvent OnCreating() { return Creating.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnCreated() { return Created.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnTaskProducing() { return TaskProducing.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnTaskProduced() { return TaskProduced.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnNewTaskCreated() { return NewTaskCreated.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnCompleteTaskRemoved() { return CompleteTaskRemoved.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnCapacityExceeded() { return CapacityExceeded.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnQueueTryAddError() { return QueueTryAddError.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnTaskConsuming() { return TaskConsuming.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnTaskConsumed() { return TaskConsumed.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnDisposing() { return Disposing.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnDisposed() { return Disposed.New.WithPrefix("\t\tQueueEvents."); }
        public static IEvent OnExceptionHandled(Exception ex) { return ExceptionHandled.New(ex).WithPrefix("\t\tQueueEvents."); }
    }
}
