﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Oxp.ServiceModel.Patterns
{
    using Events;
    using Common;
    using Context;

    public sealed class ProducerConsumerQueue : IDisposable
    {
        private BlockingCollection<Task> _queue;
        private SemaphoreSlim _semaphore;
        private Task _consumersDispatcherTask;

        public IProducerConsumerQueueContext Context { get; private set; }

        public ProducerConsumerQueue() : this(ProducerConsumerQueueContext.Default) { }

        public ProducerConsumerQueue(IProducerConsumerQueueContext context)
        {
            using (context.InScopeOf(QueueEvents.OnCreating, QueueEvents.OnCreated))
            {
                Context = (IProducerConsumerQueueContext)context.With(Thread.CurrentThread);
                _queue = new BlockingCollection<Task>(Context.MaxQueueCapacity);
                _semaphore = new SemaphoreSlim(Context.MaxConsumersCount);
                _consumersDispatcherTask = CreateDispatcherTask();
            }
        }

        public void Enqueue(object data, Action<ITaskContext> action)
        {
            using (Context.InScopeOf(QueueEvents.OnTaskProducing, QueueEvents.OnTaskProduced))
            {
                if (Context.MaxQueueCapacity == _queue.Count)
                {
                    Context.EventBus.Publish(Events.QueueEvents.OnCapacityExceeded());
                }

                var tc = TaskContext.New.With(data)
                                        .With(Thread.CurrentThread, Context.CancellationToken)
                                        .With(Context.EventBus);
                var task = CreateConsumerTask(tc, action);
                Context.EventBus.Publish(Events.QueueEvents.OnNewTaskCreated());

                // it should block execution until _queue.Count doesn't exceed  _queue.Capacity
                if (_queue.TryAdd(task, int.MaxValue, Context.CancellationToken))
				{
					Context.EventBus.Publish(TaskEvents.OnTraceVerbose(string.Format("{3}>>> [D:{0:0000}][Q:{1}][W:{2}]", data, _queue.Count, ThreadsCount, Environment.NewLine)));
				}
				else
                {
					// if we reached this than _queue can't take a new data during the int.MaxValue ms,
                    // it should be something wrong.
                    Context.EventBus.Publish(Events.QueueEvents.OnQueueTryAddError());
                }
            }
        }

        public int Count { get { return _queue.Count; } }

        public int ThreadsCount { get { return Context.MaxConsumersCount - _semaphore.CurrentCount; } }

        public void Dispose()
        {
            using (Context.InScopeOf(QueueEvents.OnDisposing, QueueEvents.OnDisposed))
            {
                _queue.CompleteAdding();

                if (Context.ClearQueueOnDispose)
                {
                    _queue.Dispose();
                }

                if (Context.WaitConsumersOnDispose)
                {
                    var locker = new object();
                    lock (locker)
                    {
                        while (ThreadsCount > 0)
                            Monitor.Wait(locker, Context.MaxIdleTimeout);
                    }
                }
            }
        }

        private Task CreateConsumerTask(ITaskContext tc, Action<ITaskContext> action)
        {
            return new Task((o) =>
            {
                var _tc = (ITaskContext)o;
                using (_tc.InScopeOf(QueueEvents.OnTaskConsuming, QueueEvents.OnTaskConsumed))
                {
                    var token = _tc.CancellationToken;

                    token.ThrowIfCancellationRequested();

                    try
                    {
                        action(_tc);
                    }
                    catch (Exception ex)
                    {
                        _tc.EventBus.Publish(TaskEvents.OnExceptionHandled(ex));
                    }
                }
            }
            , (object)tc
            , Context.CancellationToken
            , TaskCreationOptions.None);
        }

        private Task CreateDispatcherTask()
        {
            return Task.Factory.StartNew((object o) =>
            {
                var context = (IProducerConsumerQueueContext)o;
                foreach (var task in _queue.GetConsumingEnumerable())
                {
					_semaphore.Wait(context.CancellationToken);
					context.CancellationToken.ThrowIfCancellationRequested();

					task.ContinueWith(Catch
									, context.CancellationToken
									, TaskContinuationOptions.OnlyOnFaulted
									, TaskScheduler.Default)
						.ContinueWith(Finally
									, context.CancellationToken
									, TaskContinuationOptions.None
									, TaskScheduler.Default);

					task.Start(TaskScheduler.Default);
                }
            }
            , (object)Context
            , Context.CancellationToken
            , TaskCreationOptions.LongRunning
            , TaskScheduler.Default)
            .ContinueWith(Catch
                        , Context.CancellationToken
                        , TaskContinuationOptions.OnlyOnFaulted
                        , TaskScheduler.Default);
        }

        private void Catch(Task task)
        {
            task.Exception.Flatten().Handle(ex =>
            {
                Context.EventBus.Publish(Events.QueueEvents.OnExceptionHandled(ex));
                return true;
            });
        }

        private void Finally(Task task)
        {
            _semaphore.Release();
            Context.EventBus.Publish(Events.QueueEvents.OnCompleteTaskRemoved());
        }
    }
}
