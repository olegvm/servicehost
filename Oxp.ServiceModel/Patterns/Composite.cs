﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Patterns
{
    public class Composite<T> : ICollection<T>
    {
        private ICollection<T> _list = new List<T>();

        public bool SuppressExceptions = false;

        public event EventHandler<Exception> ExceptionHandled = (o, e) => { };

        public virtual void Invoke(Action<T> action)
        {
            foreach (var item in _list)
            {
                InvokeCore(item, action);
            }
        }

        protected void InvokeCore<D>(D data, Action<D> action)
        {
            try
            {
                action(data);
            }
            catch (Exception ex)
            {
                ExceptionHandled(this, ex);
                if (!SuppressExceptions)
                    throw;
            }
        }

        public void Add(T item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return _list.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            return _list.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
