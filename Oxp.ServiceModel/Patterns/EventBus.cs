﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Oxp.ServiceModel.Patterns
{
    using Events;

    public class EventBus : IEventBus, IPublisher, ISubscriber, IDisposable
    {
        private readonly IScheduler _scheduler;
        private readonly ICollection<IDisposable> _subscriptions = new List<IDisposable>();
        private readonly ISubject<IEvent> _subject = new Subject<IEvent>();

        #region .ctor-s

        public EventBus() : this(new NewThreadScheduler()) { } //this(TaskPoolScheduler.Default)
        public EventBus(IScheduler scheduler) { _scheduler = scheduler; }

        #endregion

        public IPublisher Publish<T>(T @event) where T : IEvent
        {
            _subject.OnNext(@event);

            return this;
        }

        public ISubscriber Subscribe<T>(Action<T> onNext) where T : IEvent
        {
            _subscriptions.Add(_subject.Where(t => t is T).Cast<T>()
                                .ObserveOn(_scheduler)
                                .Subscribe(onNext));

            return this;
        }

        public ISubscriber Subscribe<T>(Action<T> onNext, Action<Exception> onError) where T : IEvent
        {
            _subscriptions.Add(_subject.Where(t => t is T).Cast<T>()
                                .ObserveOn(_scheduler)
                                .Subscribe(onNext, onError));
            return this;
        }

        public void Dispose()
        {
            _subject.OnCompleted();
            (_subject as IDisposable).Dispose();

            if (_subscriptions != null)
                foreach (var subscription in _subscriptions)
                    subscription.Dispose();
        }
    }
}