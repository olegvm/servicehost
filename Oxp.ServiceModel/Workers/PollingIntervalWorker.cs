﻿using System;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Workers
{
    using Context;

    public abstract class PollingIntervalWorker<T> : WorkerBase<T>, IWorker, IDisposable
        where T : WorkerBase<T>, IWorker, IDisposable, new()
    {
        private Patterns.ProducerConsumerQueue _queue;

        #region IWorker Members

        public override void Initialize() 
        {
            var context = ProducerConsumerQueueContext.Default
                                                        .With(Context.EventBus)
                                                        .With(null, Context.CancellationToken);
            _queue = new Patterns.ProducerConsumerQueue(context);
        }

        public override void PerformWork(object data)
        {
            _queue.Enqueue(data, this.ExecuteTask);
        }

        #endregion

        #region IDisposable Members

        public sealed override void Dispose() 
        {
            OnDispose();
            _queue.Dispose();
        }

        #endregion

        protected virtual void OnDispose() { }

        protected abstract void ExecuteTask(ITaskContext context);
    }
}
