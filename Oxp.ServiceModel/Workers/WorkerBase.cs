﻿using System;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Workers
{
    using Common;
    using Context;
    using Events;

    public abstract class WorkerBase<T> : IWorker, IDisposable where T : WorkerBase<T>, IWorker, new()
    {
        public static IWorker Create(IWorkerContext context)
        {
            using(context.InScopeOf(WorkerEvents.OnCreating, WorkerEvents.OnCreated))
                return Behaviors.WorkerBehavior.Create(new T() { Context = context });
        }

        public static IWorker Create()
        {
            return Create(WorkerContext.New);
        }

        #region IWorker Members

        public Context.IWorkerContext Context { get; set; }

        public abstract void Initialize();

        public virtual bool WaitForWork()
        {
            return !Context.CancellationToken.WaitHandle.WaitOne(Context.PollInterval);
        }

        public virtual IEnumerable<object> ReceiveWork() 
        { 
            yield return null; 
            yield break;
        }

        public abstract void PerformWork(object data);

        #endregion

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}
