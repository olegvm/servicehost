﻿using System;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Workers
{
    public class SecondsCounterWorker : PollingIntervalWorker<SecondsCounterWorker>, IWorker, IDisposable
    {
        private int _counter = 0;

        #region IWorker Members

        public override void Initialize() 
        {
            base.Initialize(); // <- to set context options.
            Context.PollInterval = TimeSpan.FromSeconds(1);
        }

        public override IEnumerable<object> ReceiveWork()
        {
            _counter++;

            yield return _counter;
			System.Threading.Thread.Sleep(100);
            yield return _counter + 1000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 2000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 3000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 4000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 5000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 6000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 7000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 8000;
			System.Threading.Thread.Sleep(100);
			yield return _counter + 9000;
            yield break;
        }

        #endregion

        #region PollingIntervalWorker Members

        protected override void ExecuteTask(Context.ITaskContext context)
        {
            var rnd = new Random();
            System.Threading.Thread.Sleep(5000); //rnd.Next(1, 4) * 1000);

            if ((_counter % 5) == 4)
                throw new ApplicationException(context.Data.ToString());

            var str = string.Format("{1}<<< [D:{0:0000}]", context.Data, Environment.NewLine);

            context.EventBus.Publish(Events.TaskEvents.OnTrace(str));
        }

        #endregion
    }
}
