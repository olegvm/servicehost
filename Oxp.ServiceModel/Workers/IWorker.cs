﻿using System;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Workers
{
    public interface IWorker : IDisposable
    {
        Context.IWorkerContext Context { get; set; }

        void Initialize();
        bool WaitForWork();
        IEnumerable<object> ReceiveWork();
        void PerformWork(object data);
    }
}
