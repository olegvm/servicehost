﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Oxp.ServiceModel.Services
{
    using Common;
    using Context;
    using Events;

    public sealed class ServiceController : Services.IService
    {
        private Task _task;
        private CancellationTokenSource _canceller;
        private ManualResetEventSlim _pause = new ManualResetEventSlim(true);
        private IServiceContext _context;

        public IServiceContext Context
        {
            get { return _context; }
        }

        public IService With(string[] args)
        {
            InitializeContext(args);

            return this;
        }

        public void Start()
        {
            if (!_pause.Wait(0))
            {
                _pause.Set();
                return;
            }

            if (_task == null)
            {
                _canceller = new CancellationTokenSource();
                _context.CancellationToken = _canceller.Token;
                _task = Task.Factory.StartNew(ThreadMethod
                                                , (object)_context
                                                , _context.CancellationToken
                                                , TaskCreationOptions.LongRunning
                                                , TaskScheduler.Default);
            }
            else
            {
                throw new ApplicationException("Can't start yet another task for service. Wait for the previous one to complete please.");
            }
        }

        public void Pause()
        {
            _pause.Reset();
        }

        public void Continue()
        {
            _pause.Set();
        }

        public void Stop()
        {
            _pause.Set(); // <- just to force stopping from the paused state.
            _canceller.Cancel();

            if (_task != null)
            {
                try
                {
                    var @continue = _task.ContinueWith(t =>
                    {
                        if (t.IsFaulted)
                            foreach (var e in t.Exception.InnerExceptions)
                                _context.Publish(ServiceEvents.OnExceptionHandled(e));
                    }
                    , TaskContinuationOptions.NotOnRanToCompletion);

                    @continue.Wait();
                }
                catch (AggregateException ex)
                {
                    ex.Handle(e =>
                    {
                        _context.Publish(ServiceEvents.OnExceptionHandled(e));
                        return true;
                    });
                }
                finally
                {
                    _task = null;
                }
            }
        }

        public void Dispose() { }

        private void InitializeContext(string[] args)
        {
            _context = Configuration.ContextBuilder.Service(args);
        }

        private void ThreadMethod(object o)
        {
            ThreadMethodBehavior(context => 
            {
                using (var worker = context.CreateWorker())
                {
                    worker.Initialize();
                    while (worker.WaitForWork())
                    {
                        _pause.Wait(context.CancellationToken);
                        context.CancellationToken.ThrowIfCancellationRequested();
                        foreach (var data in worker.ReceiveWork())
                        {
                            worker.PerformWork(data);
                        }
                    }
                }
            }
            , (IServiceContext)o);
        }

        private void ThreadMethodBehavior(Action<IServiceContext> body, IServiceContext context)
        {
            context.Publish(ThreadEvents.OnThreadStart());
            try
            {
                while (!context.CancellationToken.IsCancellationRequested)
                {
                    body(context);
                }

                context.CancellationToken.ThrowIfCancellationRequested();
            }
            catch (StackOverflowException)
            {
                throw;
            }
            catch (ThreadAbortException)
            {
                context.Publish(ThreadEvents.OnAbort());
                throw;
            }
            catch (OperationCanceledException)
            {
                context.Publish(ThreadEvents.OnCancel());
                throw;
            }
            catch (Exception ex)
            {
                context.Publish(ThreadEvents.OnExceptionHandled(ex));
            }
            finally
            {
                context.Publish(ThreadEvents.OnThreadComplete());
            }
        }
    }
}
