﻿using System;
using System.Diagnostics;

namespace Oxp.ServiceModel.Services
{
    public interface IService : IDisposable
    {
        Context.IServiceContext Context { get; }

        void Start();
        void Pause();
        void Continue();
        void Stop();
        IService With(string[] args);
    }
}
