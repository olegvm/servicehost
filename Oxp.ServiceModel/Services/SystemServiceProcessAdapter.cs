﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace Oxp.ServiceModel.Services
{
    public sealed class SystemServiceProcessAdapter : ServiceBase, IService, IDisposable
    {
        private IService _service;

        public SystemServiceProcessAdapter With(IService service)
        {
            this._service = service;

            var context = service.Context;
            this.ServiceName = context.ServiceName;
            this.AutoLog = context.AutoLog;
            this.CanHandlePowerEvent = context.CanHandlePowerEvent;
            this.CanHandleSessionChangeEvent = context.CanHandleSessionChangeEvent;
            this.CanPauseAndContinue = context.CanPauseAndContinue;
            this.CanShutdown = context.CanShutdown;
            this.CanStop = context.CanStop;

            return this;
        }

        public static SystemServiceProcessAdapter Create(IService service)
        {
            return new SystemServiceProcessAdapter().With(service);
        }

        #region ServiceBase Members

        public override EventLog EventLog
        {
            get { return _service.Context.EventLog; }
        }

        #endregion

        #region IService Members

        public Context.IServiceContext Context
        {
            get { return _service.Context; }
        }

        public IService With(string[] args)
        {
            return _service.With(args);
        }

        public void Start()
        {
            _service.Start();
        }

        public void Pause()
        {
            _service.Pause();
        }

        public void Continue()
        {
            _service.Continue();
        }

        public new void Stop()
        {
            _service.Stop();
            base.Stop();
        }

        #endregion

        #region IDisposable Members

        public new void Dispose()
        {
            _service.Dispose();
            base.Dispose();
        }

        #endregion
    }
}
