﻿using System;
using System.Diagnostics;

namespace Oxp.ServiceModel.Behaviors
{
    using Common;
    using Events;

    public sealed class ServiceBehavior : Services.IService
    {
        private Services.IService _service;

        public Services.IService With(Services.IService service)
        {
            this._service = service;

            return this;
        }

        #region IService Members

        public Context.IServiceContext Context
        {
            get 
            {
                return _service.Context;
            }
        }

        public Services.IService With(string[] args)
        {
            return _service.With(args);
        }

        public void Start()
        {
            try
            {
                using (Context.InScopeOf(ServiceEvents.OnStarting, ServiceEvents.OnStarted))
                {
                    _service.Start();
                    _stopped = false;
                }
            }
            catch (Exception ex)
            {
                Context.Publish(ServiceEvents.OnExceptionHandled(ex));
            }
        }

        public void Pause()
        {
            try
            {
                using (Context.InScopeOf(ServiceEvents.OnPausing, ServiceEvents.OnPaused))
                    _service.Pause();
            }
            catch (Exception ex)
            {
                Context.Publish(ServiceEvents.OnExceptionHandled(ex));
            }
        }

        public void Continue()
        {
            try
            {
                using (Context.InScopeOf(ServiceEvents.OnContinuing, ServiceEvents.OnContinued))
                    _service.Continue();
            }
            catch (Exception ex)
            {
                Context.Publish(ServiceEvents.OnExceptionHandled(ex));
            }
        }

        private bool _stopped = false;
        public void Stop()
        {
            try
            {
                using (Context.InScopeOf(ServiceEvents.OnStopping, ServiceEvents.OnStopped))
                {
                    _service.Stop();
                    _stopped = true;
                }
            }
            catch (Exception ex)
            {
                Context.Publish(ServiceEvents.OnExceptionHandled(ex));
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                using (Context.InScopeOf(ServiceEvents.OnDisposing, ServiceEvents.OnDisposed))
                {
                    if (!_stopped) this.Stop();
                    foreach (var ext in _service.Context.Extensions)
                        ext.Dispose();
                    _service.Dispose();
                }
            }
            catch (Exception ex)
            {
                Context.Publish(ServiceEvents.OnExceptionHandled(ex));
            }
            finally
            {
                Context.EventBus.Dispose(); // <- to release thread under NewThreadScheduler.
            }
        }

        #endregion
    }
}
