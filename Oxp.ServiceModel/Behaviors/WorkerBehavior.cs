﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Behaviors
{
    using Common;
    using Events;
    using Workers;

    public sealed class WorkerBehavior : IWorker, IDisposable
    {
        private IWorker _worker;

        #region IWorker Members

        public Context.IWorkerContext Context
        {
            get { return _worker.Context; }
            set { _worker.Context = value; }
        }

        public void Initialize()
        {
            using (Context.InScopeOf(WorkerEvents.OnInitializing, WorkerEvents.OnInitialized))
            {
                SafeInvoke(_worker.Initialize);
            }
        }

        public bool WaitForWork()
        {
            using (Context.InScopeOf(WorkerEvents.OnWaiting, WorkerEvents.OnWaitComplete))
            {
                return SafeInvoke<bool>(_worker.WaitForWork);
            }
        }

        public IEnumerable<object> ReceiveWork()
        {
            using (Context.InScopeOf(WorkerEvents.OnWorkReceiving, WorkerEvents.OnWorkReceived))
            {
                return SafeInvoke<IEnumerable<object>>(_worker.ReceiveWork);
            }
        }

        public void PerformWork(object data)
        {
            using (Context.InScopeOf(WorkerEvents.OnWorkPerforming, WorkerEvents.OnWorkPerformed))
            {
                SafeInvoke(_worker.PerformWork, data);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            using (Context.InScopeOf(WorkerEvents.OnDisposing, WorkerEvents.OnDisposed))
            {
                SafeInvoke(_worker.Dispose);
            }
        }

        #endregion

        public static IWorker Create(IWorker worker)
        {
            return new WorkerBehavior() { _worker = worker };
        }

        private T SafeInvoke<T>(Func<T> func)
        {
            try
            {
                Context.CancellationToken.ThrowIfCancellationRequested();
                return func();
            }
            catch (System.StackOverflowException)
            {
                throw;
            }
            catch (System.Threading.ThreadAbortException)
            {
                throw;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Context.Publish(WorkerEvents.OnExceptionHandled(ex));
                return default(T);
            }
        }

        private void SafeInvoke(Action action)
        {
            try
            {
                Context.CancellationToken.ThrowIfCancellationRequested();
                action();
            }
            catch (System.StackOverflowException)
            {
                throw;
            }
            catch (System.Threading.ThreadAbortException)
            {
                throw;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Context.Publish(WorkerEvents.OnExceptionHandled(ex));
            }
        }

        private void SafeInvoke<T>(Action<T> action, T data)
        {
            try
            {
                Context.CancellationToken.ThrowIfCancellationRequested();
                action(data);
            }
            catch (System.StackOverflowException)
            {
                throw;
            }
            catch (System.Threading.ThreadAbortException)
            {
                throw;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Context.Publish(WorkerEvents.OnExceptionHandled(ex));
            }
        }
    }
}
