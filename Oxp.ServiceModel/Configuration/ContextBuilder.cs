﻿using System;

namespace Oxp.ServiceModel.Configuration
{
    using Behaviors;
    using Extensions;
    using Context;
    using Events;
    using Workers;

    public class ContextBuilder
    {
        public static IServiceContext Service(string[] args)
        {
            var context = ServiceContext.Default.With(new Patterns.EventBus()) as IServiceContext;
            var ext = new ColoredConsoleLogger();
            ext.Initialize(context);
            context.Extensions.Add(ext);

/*
            context.EventBus
                .Subscribe<ServiceEvents.Starting>(e => {})
                .Subscribe<ServiceEvents.Started>(e => {})
                .Subscribe<ServiceEvents.Pausing>(e => {})
                .Subscribe<ServiceEvents.Paused>(e => {})
                .Subscribe<ServiceEvents.Continuing>(e => {})
                .Subscribe<ServiceEvents.Continued>(e => {})
                .Subscribe<ServiceEvents.Stopping>(e => {})
                .Subscribe<ServiceEvents.Stopped>(e => {})
                .Subscribe<ServiceEvents.Disposing>(e => {})
                .Subscribe<ServiceEvents.Disposed>(e => {})
                .Subscribe<ServiceEvents.ExceptionHandled>(e => {})
                .Subscribe<WorkerEvents.Created>(e => {})
                .Subscribe<WorkerEvents.Creating>(e => {})
                .Subscribe<WorkerEvents.Initializing>(e => {})
                .Subscribe<WorkerEvents.Initialized>(e => {})
                .Subscribe<WorkerEvents.Waiting>(e => {})
                .Subscribe<WorkerEvents.WaitComplete>(e => {})
                .Subscribe<WorkerEvents.WorkReceiving>(e => {})
                .Subscribe<WorkerEvents.WorkReceived>(e => {})
                .Subscribe<WorkerEvents.WorkPerforming>(e => {})
                .Subscribe<WorkerEvents.WorkPerformed>(e => {})
                .Subscribe<WorkerEvents.ExceptionHandled>(e => {})
                .Subscribe<ThreadEvents.ThreadStart>(e => {})
                .Subscribe<ThreadEvents.Cancel>(e => {})
                .Subscribe<ThreadEvents.Abort>(e => {})
                .Subscribe<ThreadEvents.ThreadComplete>(e => {})
                .Subscribe<ThreadEvents.ExceptionHandled>(e => {})
                .Subscribe<QueueEvents.Created>(e => {})
                .Subscribe<QueueEvents.Creating>(e => {})
                .Subscribe<QueueEvents.TaskProducing>(e => {})
                .Subscribe<QueueEvents.TaskProduced>(e => {})
                .Subscribe<QueueEvents.NewTaskCreated>(e => {})
                .Subscribe<QueueEvents.CompleteTaskRemoved>(e => {})
                .Subscribe<QueueEvents.CapacityExceeded>(e => {})
                .Subscribe<QueueEvents.QueueTryAddError>(e => {})
                .Subscribe<QueueEvents.TaskConsuming>(e => {})
                .Subscribe<QueueEvents.TaskConsumed>(e => {})
                .Subscribe<QueueEvents.Disposing>(e => {})
                .Subscribe<QueueEvents.Disposed>(e => {})
                .Subscribe<QueueEvents.ExceptionHandled>(e => {})
                .Subscribe<TaskEvents.Trace>(e => {})
                .Subscribe<TaskEvents.TraceVerbose>(e => {})
                .Subscribe<TaskEvents.TraceInformation>(e => {})
                .Subscribe<TaskEvents.TraceWarning>(e => {})
                .Subscribe<TaskEvents.TraceError>(e => {})
                .Subscribe<TaskEvents.ExceptionHandled>(e => {});
*/
            return context;
        }
    }
}
