﻿using System;
using System.IO;

namespace Oxp.ServiceModel.Extensions
{
    using Context;
    using Events;
    using Patterns;

    public abstract class ServiceContextLogger 
        : ServiceContextExtensionBase, IServiceContextExtension, IExtension<IServiceContext>, IDisposable
    {
        private TextWriter _writer;

        #region .ctor-s

        public ServiceContextLogger() { }
        public ServiceContextLogger(TextWriter writer) { _writer = writer; }

        #endregion

        #region ServiceContextExtensionBase members

        protected override void OnEvent(IEvent @event)
        {
            _writer.WriteLine(@event.ToString());
        }

        #endregion

        #region IExtension<IServiceContext> Members

        public sealed override void Initialize(IServiceContext context)
        {
            base.Initialize(context);
        }

        #endregion

        #region IDisposable members

        public sealed override void Dispose()
        {
            base.Dispose();
        }

        #endregion
    }
}
