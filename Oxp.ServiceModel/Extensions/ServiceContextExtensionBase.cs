﻿using System;

namespace Oxp.ServiceModel.Extensions
{
    using Events;
    using Context;

    public abstract class ServiceContextExtensionBase : IServiceContextExtension, IExtension<IServiceContext>, IDisposable
    {
        protected virtual void OnInitialize(IServiceContext context) { }

        protected abstract void OnEvent(IEvent @event);

        protected virtual void OnException(Exception ex)
        {
            ServiceContext.EventBus.Publish(ExtensionEvents.OnExceptionHandled(ex));
        }

        protected abstract void OnDispose();

        #region IServiceContextExtension Members

        public IServiceContext ServiceContext { get; private set; }

        #endregion

        #region IExtension<IServiceContext> Members

        public virtual void Initialize(IServiceContext context)
        {
            ServiceContext = context;
            ServiceContext.EventBus.Subscribe<IEvent>(OnEvent, OnException);
            OnInitialize(ServiceContext);
        }

        #endregion

        #region IDisposable Members

        public virtual void Dispose() 
        {
            OnDispose();
        }

        #endregion
    }
}
