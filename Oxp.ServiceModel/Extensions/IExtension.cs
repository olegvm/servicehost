﻿using System;

namespace Oxp.ServiceModel.Extensions
{
    public interface IExtension<T> : IDisposable
    {
        void Initialize(T instance);
    }
}
