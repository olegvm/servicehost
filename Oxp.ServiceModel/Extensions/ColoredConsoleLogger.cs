﻿using System;
using System.IO;

namespace Oxp.ServiceModel.Extensions
{
    using Context;
    using Events;
    using Patterns;

    public class ColoredConsoleLogger 
        : ServiceContextLogger, IServiceContextExtension, IExtension<IServiceContext>, IDisposable
    {
        private ConsoleColor _color;

        #region .ctor

        public ColoredConsoleLogger() : base(Console.Out) { _color = Console.ForegroundColor; }

        #endregion

        #region ServiceContextExtensionBase members

        protected override void OnEvent(IEvent @event)
        {
                 if (@event is IServiceEvent) Console.ForegroundColor = ConsoleColor.Gray;
            else if (@event is IThreadEvent) Console.ForegroundColor = ConsoleColor.White;
            else if (@event is IWorkerEvent) Console.ForegroundColor = ConsoleColor.Yellow;
            else if (@event is IQueueEvent) Console.ForegroundColor = ConsoleColor.Cyan;
            else if (@event is ITaskEvent) Console.ForegroundColor = ConsoleColor.Green;
            else if (@event is IExceptionEvent) Console.ForegroundColor = ConsoleColor.Red;

            base.OnEvent(@event);
        }

        protected override void OnDispose()
        {
            Console.ForegroundColor = _color;
        }

        #endregion
    }
}
