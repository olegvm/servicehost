﻿using System;

namespace Oxp.ServiceModel.Extensions
{
    using Context;

    public interface IServiceContextExtension: IExtension<IServiceContext>
    {
        IServiceContext ServiceContext { get; }
    }
}
