﻿using System;

namespace Oxp.ServiceModel.Common
{
    public class DisposableScope: IDisposable
    {
        private Action _onDispose;
        private Action<Exception> _onError;

        public DisposableScope(Action onCreate, Action onDispose, Action<Exception> onError = null)
        {
            _onDispose = onDispose;
            _onError = onError;

            InvokeCore(onCreate);
        }

        #region IDisposable Members

        public void Dispose()
        {
            InvokeCore(_onDispose);
        }

        #endregion

        private void InvokeCore(Action action)
        {
            try
            {
                if (action != null) 
                    action();
            }
            catch (Exception ex)
            {
                if (_onError == null)
                    throw;
                else
                    _onError(ex);
            }
        }
    }
}
