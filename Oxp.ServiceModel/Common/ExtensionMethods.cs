﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Common
{
    using Context;
    using Events;
    using Workers;

    public static class ExtensionMethods
    {
        public static void Publish<T>(this IEventBusContext context, T @event) where T : IEvent
        {
            context.EventBus.Publish<T>(@event);
        }

        public static IWorker CreateWorker(this IServiceContext context)
        {
            return SecondsCounterWorker.Create(WorkerContext.New
                                                   .With(TimeSpan.FromSeconds(3))
                                                   .With(Thread.CurrentThread, context.CancellationToken)
                                                   .With(context.EventBus) as IWorkerContext);
        }

        public static IDisposable InScopeOf(this IEventBusContext context, Func<IEvent> @in, Func<IEvent> @out)
        {
            return new DisposableScope(() => context.EventBus.Publish(@in())
                                      ,() => context.EventBus.Publish(@out()));
        }

        public static void Raise<T>(this EventHandler<T> handler, object sender, T data)
        {
            var h = handler;
            if (h == null) return;
            h(sender, data);
        }
    }
}
