﻿using System;
using System.Collections.Generic;

namespace Oxp.ServiceModel.Common
{
    public class CompositeMethod<T> : Patterns.Composite<Action<T>>, ICollection<Action<T>>
    {
        public void Invoke(T data)
        {
            foreach (var item in this)
            {
                InvokeCore(data, item);
            }
        }
    }
}
