﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Oxp.ServiceModel.Context
{
    using Extensions;

    public interface IServiceContext : IThreadContext, IEventBusContext, IContext, IDisposable
    {
        string ServiceName { get; }
        bool AutoLog { get; }
        bool CanHandlePowerEvent { get; }
        bool CanHandleSessionChangeEvent { get; }
        bool CanPauseAndContinue { get; }
        bool CanShutdown { get; }
        bool CanStop { get; }
        EventLog EventLog { get; }
        ICollection<IExtension<IServiceContext>> Extensions { get; }
    }                                   
}
