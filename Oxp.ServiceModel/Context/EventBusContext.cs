﻿using System;

namespace Oxp.ServiceModel.Context
{
    using Events;

    public class EventBusContext<T> : ContextBase<T>, IEventBusContext, IContext, IDisposable
        where T : EventBusContext<T>, IEventBusContext, IContext, IDisposable, new()
    {
        #region IEventBusContext members

        public IEventBus EventBus { get; set; }

        #endregion

        #region IDisposable members

        public sealed override void Dispose() 
        {
            OnDispose();
        }

        #endregion

        protected virtual void OnDispose() { }
    }
}
