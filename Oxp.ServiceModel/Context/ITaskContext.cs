﻿using System;

namespace Oxp.ServiceModel.Context
{
    public interface ITaskContext : IThreadContext, IEventBusContext
    {
        object Data { get; set; }
    }
}
