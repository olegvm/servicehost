﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Context
{
    public static class FluentInteface
    {
        public static T With<T>(this T context, Events.IEventBus instance = null) where T : IEventBusContext
        {
            context.EventBus = instance ?? new Patterns.EventBus();

            return context;
        }

        public static T With<T>(this T context, Thread thread = null, CancellationToken token = default(CancellationToken))
            where T : IThreadContext
        {
            context.Thread = thread ?? Thread.CurrentThread;
            context.CancellationToken = token;

            return context;
        }
    }
}
