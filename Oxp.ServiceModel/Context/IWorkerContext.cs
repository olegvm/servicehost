﻿using System;

namespace Oxp.ServiceModel.Context
{
    public interface IWorkerContext : IThreadContext, IEventBusContext, IContext, IDisposable
    {
        TimeSpan PollInterval { get; set; }
    }
}
