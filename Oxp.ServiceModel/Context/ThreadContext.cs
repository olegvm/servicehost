﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Context
{
    public class ThreadContext<T> : EventBusContext<T>, IThreadContext, IEventBusContext, IContext, IDisposable
        where T : ThreadContext<T>, IThreadContext, IEventBusContext, IContext, IDisposable, new()
    {
        #region IThreadContext members

        public Thread Thread { get; set; }
        public CancellationToken CancellationToken { get; set; }

        #endregion
    }
}
