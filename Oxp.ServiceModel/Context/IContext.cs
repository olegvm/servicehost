﻿using System;

namespace Oxp.ServiceModel.Context
{
    public interface IContext : IDisposable 
    {
        // marker interface
    }
}
