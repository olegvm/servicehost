﻿using System;

namespace Oxp.ServiceModel.Context
{
    public interface IEventBusContext : IContext, IDisposable
    {
        Events.IEventBus EventBus { get; set; }
    }
}
