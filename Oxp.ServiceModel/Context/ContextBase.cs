﻿using System;

namespace Oxp.ServiceModel.Context
{
    public abstract class ContextBase<T> : IContext, IDisposable 
        where T : ContextBase<T>, IContext, IDisposable, new()
    {
        #region IDisposable members

        public abstract void Dispose();

        #endregion

        public static T New
        {
            get { return new T(); }
        }
    }
}
