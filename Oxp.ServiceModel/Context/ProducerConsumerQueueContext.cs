﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Context
{
    public class ProducerConsumerQueueContext : ThreadContext<ProducerConsumerQueueContext>, IProducerConsumerQueueContext, IThreadContext, IEventBusContext, IContext, IDisposable
    {
        #region IProducerConsumerQueueContext members

        public int  MaxConsumersCount { get; set; }
        public int  MaxQueueCapacity { get; set; }
        public int  MaxIdleTimeout { get; set; }
        public bool ClearQueueOnDispose { get; set; }
        public bool WaitConsumersOnDispose { get; set; }

        #endregion

        public static ProducerConsumerQueueContext Default
        {
            get
            {
                return new ProducerConsumerQueueContext 
                {
                    MaxConsumersCount = Environment.ProcessorCount,
                    MaxQueueCapacity = Environment.ProcessorCount / 2, //int.MaxValue,
                    MaxIdleTimeout = 1000,
                    ClearQueueOnDispose = false,
                    WaitConsumersOnDispose = true
                };
            }
        }
    }
}
