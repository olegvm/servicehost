﻿using System;

namespace Oxp.ServiceModel.Context
{
    public interface IProducerConsumerQueueContext : IThreadContext, IEventBusContext, IContext, IDisposable
    {
        int  MaxConsumersCount { get; set; }
        int  MaxQueueCapacity { get; set; }
        int  MaxIdleTimeout { get; set; }
        bool ClearQueueOnDispose { get; set; }
        bool WaitConsumersOnDispose { get; set; }
    }
}
