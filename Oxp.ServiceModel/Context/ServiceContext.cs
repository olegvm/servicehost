﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Oxp.ServiceModel.Context
{
    using Common;
    using Extensions;
    using Workers;

    public class ServiceContext : ThreadContext<ServiceContext>, IServiceContext, IThreadContext, IEventBusContext, IContext, IDisposable
    {
        #region IServiceContext members

        public string ServiceName { get; set; }
        public bool AutoLog { get; set; }
        public bool CanHandlePowerEvent { get; set; }
        public bool CanHandleSessionChangeEvent { get; set; }
        public bool CanPauseAndContinue { get; set; }
        public bool CanShutdown { get; set; }
        public bool CanStop { get; set; }
        public EventLog EventLog { get; private set; }
        public ICollection<IExtension<IServiceContext>> Extensions { get; private set; }

        #endregion

        public IServiceContext With(string serviceName, string logName = null)
        {
            this.ServiceName = serviceName;
            this.EventLog = new EventLog { Log = logName ?? "Application", Source = serviceName };

            return this;
        }

        public static ServiceContext Default
        {
            get
            {
                return new ServiceContext
                {
                    ServiceName = "ServiceName1",
                    AutoLog = true,
                    CanHandlePowerEvent = false,
                    CanHandleSessionChangeEvent = false,
                    CanPauseAndContinue = true,
                    CanShutdown = true,
                    CanStop = true,
                    EventLog = new EventLog { Log = "Application", Source = "ServiceName1" },
                    Extensions = new List<IExtension<IServiceContext>>()
                };
            }
        }
    }
}
