﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Context
{
    public class WorkerContext : ThreadContext<WorkerContext>, IWorkerContext, IThreadContext, IEventBusContext, IContext, IDisposable
    {
        #region IWorkerContext Members

        public TimeSpan PollInterval { get; set; }

        #endregion

        public WorkerContext With(TimeSpan pollInterval)
        {
            this.PollInterval = pollInterval;

            return this;
        }
    }
}
