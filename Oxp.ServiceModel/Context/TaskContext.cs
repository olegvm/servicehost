﻿using System;

namespace Oxp.ServiceModel.Context
{
    public class TaskContext : ThreadContext<TaskContext>, ITaskContext, IThreadContext, IEventBusContext
    {
        #region ITaskContext members

        public object Data { get; set; }

        #endregion

        public ITaskContext With(object data = null)
        {
            this.Data = data;

            return this;
        }

    }
}
