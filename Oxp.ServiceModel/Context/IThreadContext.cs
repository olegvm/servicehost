﻿using System;
using System.Threading;

namespace Oxp.ServiceModel.Context
{
    public interface IThreadContext : IEventBusContext, IContext, IDisposable
    {
        Thread Thread { get; set; }
        CancellationToken CancellationToken { get; set; }
    }
}
