﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Topshelf.ServiceHost
{
    public sealed class ProducerConsumerQueue<T>: IDisposable
    {
        private PCQueueOptions _options;
        private object _locker = new object();
        private Queue<Tuple<Action<T>, T>> _queue = new Queue<Tuple<Action<T>, T>>();
        private ICollection<Thread> _threads = new List<Thread>();
        private int _tasksCount = 0;
        private CancellationTokenSource _tokenSource = new CancellationTokenSource();

        public ProducerConsumerQueue() : this(PCQueueOptions.Default) { }

        public ProducerConsumerQueue(PCQueueOptions options)
        {
            _options = options;
        }

        public void Enqueue(Action<T> action, T data)
        {
            lock (_locker)
            {
Console.WriteLine("\t>>>\t[T:{3}][Q:{1}][W:{2}][D:{0}]", data, _queue.Count, _tasksCount
    , Thread.CurrentThread.ManagedThreadId);

                _queue.Enqueue(Tuple.Create(action, data));

                //if (_queue.Count > 0 && _threads.Count < _options.MaxThreadsCount)
                //{
                //    _threads.Add(StartNewThread(_threads.Count));
                //}

                if (_queue.Count > 0 && _tasksCount < _options.MaxThreadsCount)
                    StartNewTask();

                //Monitor.Pulse(_locker);
            }
        }

        public int Count { get { return _queue.Count; } }

        private void ConsumeTask(object o)
        {
            var token = (CancellationToken)o;
            while (true)
            {
                Tuple<Action<T>, T> item;

                lock (_locker)
                {
                    if (_queue.Count == 0 || token.IsCancellationRequested) return;

                    item = _queue.Dequeue();
                }

                item.Item1(item.Item2);
            }
        }

        private void Consume()
        {
            while (true)
            {           
                Tuple<Action<T>,T> item;
                
                lock (_locker)
                {
                    while (_queue.Count == 0) Monitor.Wait(_locker);
                    item = _queue.Dequeue();
                }

                if (item.Item1 == null) return;

                item.Item1(item.Item2);
            }
        }

        private void StartNewTask()
        {
            var task = new Task(ConsumeTask, (object)_tokenSource.Token);
            task.ContinueWith(t => Console.WriteLine("[A:{0}]", --_tasksCount));
            task.Start();
            _tasksCount++;
        }

        private Thread StartNewThread(int threadsCount)
        {
            var thread = new Thread(Consume);
            thread.Name = string.Format("WorkerThread{0}", threadsCount);
            thread.IsBackground = true;
            thread.Start();

            return thread;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_options.ClearQueueOnDispose)
                lock (_locker) _queue.Clear();

            // despite the queue can be purged before we have to send return sign to worker threads.
            //foreach (var thread in _threads.ToArray())
            //    Enqueue(null, default(T));

            if (_options.WaitForThreadsOnDispose)
            {
                while (_tasksCount > 0) Thread.Sleep(100);
            }
                //foreach (var thread in _threads.ToArray())
                //    thread.Join();
        }

        #endregion

        public class PCQueueOptions
        {
            public int MaxThreadsCount { get; set; }
            public bool ClearQueueOnDispose { get; set; }
            public bool WaitForThreadsOnDispose { get; set; }

            public static PCQueueOptions Default
            {
                get
                {
                    return new PCQueueOptions
                    {
                        MaxThreadsCount = 4,
                        ClearQueueOnDispose = false,
                        WaitForThreadsOnDispose = true,
                    };
                }
            }
        }
    }
}
