﻿using System;
using Oxp.ServiceModel.Services;
using Oxp.ServiceModel.Behaviors;

namespace Topshelf.ServiceHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var c = new ServiceBehavior().With(new ServiceController().With(args)))
            {
                try
                {
                    c.Start();
                    while (true)
                    {
                        Console.WriteLine("[Q]uit, s[T]art, [S]top, [P]ause, [C]ontinue:");
                        var q = Console.ReadKey(true).KeyChar;
                        if (q == 'q')
                        {
                            break;
                        }
                        if (q == 't') c.Start();
                        if (q == 's') c.Stop();
                        if (q == 'p') c.Pause();
                        if (q == 'c') c.Continue();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("HOST EXCEPTION: [{0}].", ex.Message);
                }
            }

            Console.WriteLine("Good Bye!");
            Console.ReadKey(true);
        }
    }
}
