# ServiceHost #

В проекте реализован обобщенный **ServiceHost** на основе шаблона **ProducerConsumer**.

Также можно найти примеры использования шаблонов **Composite**, **EventBus**, **Decorator**, **Factory**, **FluentInteface**.